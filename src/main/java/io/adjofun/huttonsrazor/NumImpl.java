package io.adjofun.huttonsrazor;

public class NumImpl implements Razor<Integer> {
    @Override
    public Integer lit(int i) {
        return i;
    }

    @Override
    public Integer add(Integer left, Integer right) {
        return left + right;
    }
}
