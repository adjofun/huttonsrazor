package io.adjofun.huttonsrazor;

public interface Razor<T> {
    T lit(int i);

    T add(T left, T right);
}
