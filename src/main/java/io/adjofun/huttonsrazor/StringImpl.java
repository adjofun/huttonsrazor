package io.adjofun.huttonsrazor;

public class StringImpl implements Razor<String> {
    @Override
    public String lit(int i) {
        return i + "";
    }

    @Override
    public String add(String left, String right) {
        return "(" + left + "+" + right + ")";
    }
}
