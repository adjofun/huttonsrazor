package io.adjofun.huttonsrazor.test;

import io.adjofun.huttonsrazor.NumImpl;
import io.adjofun.huttonsrazor.Razor;
import io.adjofun.huttonsrazor.StringImpl;
import org.junit.Assert;

public class Test {

    private <T> T expr(Razor<T> f) {
        return f.add(f.lit(1), f.lit(2));
    }

    @org.junit.Test
    public void numAddition() {
        int result = this.expr(new NumImpl());
        Assert.assertEquals(3, result);
    }

    @org.junit.Test
    public void stringCatenation() {
        String result = this.expr(new StringImpl());
        Assert.assertEquals("(1+2)", result);
    }
}
